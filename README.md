This project is an MVC web application that has a single page to search for most popular tweets against any keyword.


 Features:
 It allows the user to enter a search query.
 It uses Twitter standard API to search for popular tweets against a keyword.
 It displays the tweets in bootsrap cards layout, which shows following information
  
  -Author Name
  -Author profile picture
  -Timestamp
  -Number of retweets
  -Number of favorites
  -A clickable link to the tweet on Twitter.
   

