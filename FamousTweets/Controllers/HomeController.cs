﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using FamousTweets.Models;
using System.Web.Configuration;
namespace FamousTweets.Controllers
{
    public class HomeController : Controller
    {
        private string CONSUMER_KEY = "";
        private string CONSUMER_SECRET = "";
        private string ACCESSTOKEN = "";
        private string ACCESSTOKEN_SECRET = "";


        public HomeController()
        {
            try
            {
                // Load the Consumer and Acccess keys for API calls
                CONSUMER_KEY = WebConfigurationManager.AppSettings["ConsumerKey"];
                CONSUMER_SECRET = WebConfigurationManager.AppSettings["ConsumerSecret"];
                ACCESSTOKEN = WebConfigurationManager.AppSettings["AccessToken"];
                ACCESSTOKEN_SECRET = WebConfigurationManager.AppSettings["AccessTokenSecret"];
            }
            catch (Exception)
            {


            }


        }

        public ActionResult Index()
        {
            return View();
        }


        /* ##################################### Main Tweets Function ###################################
         * 
         * The main function to request the tweets from standard twitter api and builds the wrapper
         * around the results. It finishes off by rendering a partial view for tweets.
         * 
         * #############################################################################################
         */
        public PartialViewResult SearchTweets(string query)
        {

            // Create a twitter credential object for using API calls
            ITwitterCredentials creds = new TwitterCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESSTOKEN, ACCESSTOKEN_SECRET);
            IOrderedEnumerable<ITweet> tweetsBucket = null;
            List<TweetVM> tweets = new List<TweetVM>();

            // Call the Twitter Search API with the credentials object and required search parameters
            try
            {
                if (String.IsNullOrWhiteSpace(query) == false)
                {
                    Auth.ExecuteOperationWithCredentials(creds, () =>
                    {
                        var searchParams = new SearchTweetsParameters(query) { SearchType = SearchResultType.Popular };
                        var searchTweets = Search.SearchTweets(searchParams);
                        tweetsBucket = searchTweets.OrderByDescending(tweet => tweet.CreatedAt);
                    });
                }
            }
            catch (Exception)
            {


            }

            // Build the wrapper around twitter search results
            foreach (ITweet item in tweetsBucket)
            {
                var tweet = new TweetVM()
                {
                    Tweet = item.Text,
                    TweetLink = String.Format("{0}/{1}", "https://twitter.com/statuses", item.IdStr),
                    TweetPostDateTime = String.Format("{0} at {1}", item.CreatedAt.ToLongDateString(), item.CreatedAt.ToLongTimeString()),
                    RetweetCount = item.RetweetCount,
                    FavouritesCount = item.FavoriteCount,
                    Author = String.Format("{0} <br /> <small>@{1}</small>", item.CreatedBy.Name, item.CreatedBy.ScreenName),
                    AuthorProfilePicture = item.CreatedBy.ProfileImageUrl400x400
                };

                tweets.Add(tweet);
            }

            // Render the partial with the given tweets object
            return PartialView("~/Views/Shared/PartialViews/_FamousTweets.cshtml", tweets);
        }

    }

}
