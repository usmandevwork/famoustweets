﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamousTweets.Models
{
    public class TweetVM
    {
        public string Tweet { get; set; }
        public string TweetLink { get; set; }
        public string Author { get; set; }
        public string AuthorProfilePicture { get; set; }
        public string TweetPostDateTime { get; set; }
        public int RetweetCount { get; set; }
        public int FavouritesCount { get; set; }

    }
}